Home decorating made easy with simple tips, ideas, guides and videos on decorating your home from Decorated Life. From chalk paint, milk paint, choosing the best gray color scheme, open plan decorating, bedroom decorating, lighting and the latest in kitchen design. Weekly blog posts cover the latest decorating trends and more. From farmhouse to mid century - we cover it all.

Address: 5 Sudbury Way, City Beach, WA 6015

Phone: +61 4 2521 7137
